//
//  ViewController.m
//  Translator
//
//  Created by Vladislav on 17.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import "ViewController.h"
#include "TranslateViewController.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSArray *files;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)editAction:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    files = [Utils getFiles];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    files = [Utils getFiles];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return files.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.textLabel.text = files[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"translateSegue" sender:indexPath];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [Utils deleteFileWithName:files[indexPath.row]];
    }
    [self viewWillAppear:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"translateSegue"]) {
        TranslateViewController *vc = [segue destinationViewController];
        vc.fileName = files[((NSIndexPath *)sender).row];
    }
}

- (IBAction)editAction:(id)sender {
    if ([self.tableView isEditing]) {
        [self.editButton setTitle:@"Edit"];
        [self.tableView setEditing:NO animated:YES];
    }else{
        [self.editButton setTitle:@"Done"];
        [self.tableView setEditing:YES animated:YES];
    }
}
@end
