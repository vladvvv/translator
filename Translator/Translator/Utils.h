//
//  Utils.h
//  Translator
//
//  Created by Vladislav on 17.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject


+(NSArray *)getFiles;
+(NSString *)getFileContent:(NSString *)name;
+(NSString *)getTranslateFileContent:(NSString *)name;
+(NSMutableDictionary *)getDictionary;
+(NSMutableDictionary *)getDictionaryToEng;
+(void)saveTranslate:(NSString*)name text:(NSString *)text;
+(void)saveFileWithText:(NSString *)text;
+(void)deleteFileWithName:(NSString *)name;

@end
