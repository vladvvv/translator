//
//  CreateViewController.m
//  Translator
//
//  Created by Vladislav on 17.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import "CreateViewController.h"

@interface CreateViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)saveAction:(id)sender;

@end

@implementation CreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.textView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveAction:(id)sender {
    [Utils saveFileWithText:self.textView.text];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
