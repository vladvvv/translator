//
//  AppDelegate.h
//  Translator
//
//  Created by Vladislav on 17.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

