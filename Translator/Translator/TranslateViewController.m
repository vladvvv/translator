//
//  TranslateViewController.m
//  Translator
//
//  Created by Vladislav on 17.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import "TranslateViewController.h"

@interface TranslateViewController (){
    BOOL toRus;
    NSString *fileText;
}

- (IBAction)selectLanguage:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *originalTextView;
@property (weak, nonatomic) IBOutlet UITextView *translatedTextView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *languageButton;

@end

@implementation TranslateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    toRus = YES;
    fileText = [Utils getFileContent:self.fileName];
    self.originalTextView.text = fileText;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    NSString *translate = [self translateText:fileText];
    [Utils saveTranslate:self.fileName text:translate];
    self.translatedTextView.text = translate;
}

- (NSMutableArray *)parseText:(NSString *)text{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSMutableArray *words = [NSMutableArray arrayWithArray:[text componentsSeparatedByString:@" "]];
    for (int i=0;i<words.count;i++) {
        NSString *word;
        if ([words[i] isEqualToString:@"\n"]) {
            [result addObject:@"\n"];
            continue;
        }
        if ([words[i] containsString:@"\n"]) {
            NSArray *comp = [words[i] componentsSeparatedByString:@"\n"];
            [words insertObject:comp[1] atIndex:i+1];
            [words insertObject:@"\n" atIndex:i+1];
            word = comp[0];
        }else{
            word = words[i];
        }
        [result addObject:word];
    }
    return result;
}

- (NSString *)translateText:(NSString *)text{
    NSMutableArray *parsedText = [self parseText:text];
    NSDictionary *dict;
    if (toRus) {
        dict = [Utils getDictionary];
    }else{
        dict = [Utils getDictionaryToEng];        
    }
    for (int i=0;i<parsedText.count;i++) {
        NSString *find = parsedText[i];
        NSString *symbol = [NSString string];
        if ([parsedText[i] containsString:@","] || [parsedText[i] containsString:@"."] || [parsedText[i] containsString:@":"] || [parsedText[i] containsString:@"-"] || [parsedText[i] containsString:@"!"] || [parsedText[i] containsString:@"?"]) {
            find = [parsedText[i] substringToIndex:[parsedText[i] length] - 1];
            symbol = [NSString stringWithFormat:@"%c",[parsedText[i] characterAtIndex:[parsedText[i] length]-1]];
        }
        if ([dict objectForKey:[find lowercaseString]]) {
            NSString *first = [NSString stringWithFormat:@"%C",[find characterAtIndex:0]];
            if ([first isEqualToString:[first uppercaseString]]) {
                parsedText[i] = [[[[dict objectForKey:[find lowercaseString]] stringByReplacingOccurrencesOfString:@"\n" withString:[NSString string]] stringByAppendingString:symbol] capitalizedString];
            }else{
                parsedText[i] = [[[dict objectForKey:find] stringByReplacingOccurrencesOfString:@"\n" withString:[NSString string]] stringByAppendingString:symbol];
            }
        }
    }
    NSString *result = [[parsedText componentsJoinedByString:@" "]stringByReplacingOccurrencesOfString:@"\n " withString:@"\n"];
    return result;
}

- (IBAction)selectLanguage:(id)sender {
    if (toRus) {
        [self.languageButton setTitle:@"To Eng"];
        toRus = NO;
    }else{
        [self.languageButton setTitle:@"To Ukr"];
        toRus = YES;
    }
    [self viewWillAppear:YES];
}

@end
