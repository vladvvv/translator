//
//  Utils.m
//  Translator
//
//  Created by Vladislav on 17.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(NSArray *)getFiles{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *files = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (NSString *file in files) {
        if (![file containsString:@"translate"] && ![file containsString:@"dict"]) {
            [result addObject:file];
        }
    }
    return result;
}

+(NSString *)getFileContent:(NSString *)name{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:name];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return content;
}


+(NSString *)getTranslateFileContent:(NSString *)name{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"translate-%@",name]];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return content;
}

+(NSMutableDictionary *)getDictionary{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"dict.txt"];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSArray *words = [content componentsSeparatedByString:@" "];
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    for(int i=0;i<words.count-1;i+=2){
        [result setObject:words[i] forKey:words[i+1]];
    }
    return result;
}

+(NSMutableDictionary *)getDictionaryToEng{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"dict.txt"];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSArray *words = [content componentsSeparatedByString:@" "];
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    for(int i=0;i<words.count-1;i+=2){
        [result setObject:words[i+1] forKey:words[i]];
    }
    return result;
}

+(void)saveTranslate:(NSString*)name text:(NSString *)text{
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"translate-%@", name]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    [[text dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

+(void)saveFileWithText:(NSString *)text{
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    int i = 1;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"text%d.txt", i]];
    while ([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        i++;
        fileAtPath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"text%d.txt", i]];
    }
    [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    [[text dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

+(void)deleteFileWithName:(NSString *)name{
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:name];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:fileAtPath error:nil];
    }
}

@end
